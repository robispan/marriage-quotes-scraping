from urllib2 import urlopen
from bs4 import BeautifulSoup
import random


url = 'http://quotes.yourdictionary.com/theme/marriage/'
response = urlopen(url).read()
soup = BeautifulSoup(response, "html.parser")

# find 5 paragraphs that have a class "quoteContent":
p_list = soup.find_all('p', attrs={'class': 'quoteContent'})
p_five = p_list[0:4]

for p in p_five:
    print p.string


# option 2 - using limit argument in find_all:
p_five = soup.find_all('p', attrs={'class': 'quoteContent'}, limit=5)

for p in p_five:
    print p.string


# option 3 - for 5 random quotes instead of first 5:
p_list = soup.find_all('p', attrs={'class': 'quoteContent'})

rand_list = []

while len(rand_list) < 5:
    quote = random.choice(p_list).string
    if quote not in rand_list:
        rand_list.append(quote)

for quote in rand_list:
    print quote
